<?php
class SearchApiAttachmentsIndexerAlterSettings extends SearchApiAttachmentsAlterSettings {
  /**
   * Override the getFileContent function
   * This function has been made public in order to use it in the module (Better way?)
   *
   * @param $file
   * @return bool|mixed|string
   */
  public function getFileContent($file) {
    $extraction = FALSE;
    // Let's make the variable consistent.
    $file = (array) $file;

    // Before running the (performance-intensive) extraction process, check
    // if we already have a cached copy of the extracted data.
    if (isset($file['fid'])) {
      // Load cached extraction based off file ID.
      $cid = 'cached_extraction_:' . $file['fid'];
      $cached_extraction = cache_get($cid, self::CACHE_TABLE);

      // If we have a cache hit, there really is no need to continue.
      if (!empty($cached_extraction->data)) {
        return $cached_extraction->data;
      }
    }

    if (file_exists($file['uri'])) {
      if ($file['filemime'] == 'text/plain' || $file['filemime'] == 'text/x-diff') {
        $extraction = self::extract_simple($file);
      }
      elseif (in_array($file['filemime'], array('image/jpeg', 'image/jpg', 'image/tiff'))) {
        $extraction = self::extract_exif($file);
      }
      else {
        $extraction_method = variable_get('search_api_attachments_extract_using', 'tika');
        // Send the extraction request to the right place depending on the
        // current setting.
        if ($extraction_method == 'tika') {
          $extraction = self::extract_tika($file);
        }
        else {
          $extraction = self::extract_solr($file);
        }
      }
    }
    else {
      // Log the missing file information.
      watchdog('search_api_attachments', "Couldn't index %filename content because this file was missing.", array('%filename' => $file['filename']));
    }

    // If we have actual extracted data, write it to the cache.
    if ($extraction !== FALSE && isset($cid)) {
      cache_set($cid, $extraction, self::CACHE_TABLE);
    }

    return $extraction;
  }
}